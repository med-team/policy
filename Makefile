PUBLISHHOST=alioth.debian.org
PUBLISHDIR=/home/groups/debian-med/htdocs/docs

all: policy

policy:
	rst2html policy.rst > policy.html

clean:
	rm -f policy*.html

install-deps:
	sudo apt-get install python3-docutils

